(function(globalApp) {
    //Private Property
    var me = globalApp,
    categories,
    projectPath = document.location.host + document.location.pathname;

    var listCategories = new Array();
    
    var apiConfig = {
    	mainUrl : 'https://openapi.etsy.com/v2/',
    	apikey: 'lbbqqs22o4xaofezfvnntfxx',
    	userName : 'eliasjaime89'
    };

//https://openapi.etsy.com/v2/users/testusername.js?callback=getData&api_key=your_api_key

    //Public Method
    me.init = function() {
	    console.log("init");

        urlDataHandler();
	    //me.ajax();
	    //me.getContent('taxonomy/categories',showCategories);
	    //&inludes=Images(url_75x75,url_570xN)


	    //To get all listings
	    //me.getContent('listings/active','fields=category_id,listing_id,title,price,currency_code,description,state&includes=Images(url_75x75,url_570xN)',showCategories);
 
	    //To get all categorires
	    me.getContent('taxonomy/categories','',showCategories);
	    
	    //processAjaxData();
    }
   // /v2/listings/active?fields=listing_id,title,price


    me.getContent = function(query,contentParameters,callBack){
    	contentParameters = contentParameters.length>1? contentParameters + '&': '';
    	var fullUrl = apiConfig.mainUrl + query + '.js?'+contentParameters+'api_key='+apiConfig.apikey ;
    	console.log('listCategories',listCategories);

        if(listCategories.length){
            callback(listCategories);
        }else{
            $.ajax({
                url:fullUrl,
                dataType:'jsonp',
                success: callBack
            });            
        }
    }


    //=======================================================================================

    var urlDataHandler = function(){
        var uri = new Uri(document.location);
        var listings = uri.getQueryParamValue('listings');
        if(typeof(listings)=='number'){
            //To get all categorires
            //me.getContent('taxonomy/categories','',showListings);
            
            console.log(listings);
        }else{
            console.log("no listings");
        }
    }

    var processAjaxData = function(response, urlPath){
    	document.getElementById("content").innerHTML = response.html;
    	document.title = response.pageTitle;
    	window.history.pushState({"html":response.html,"pageTitle":response.pageTitle},"", urlPath);
	}

    var showContent = function(data,contentType,behaviorSetter){
        $.ajax({
            url: 'templates/'+contentType+'.html', //ex. js/templates/mytemplate.handlebars
            cache: true,
            success: function(html) {
                var source = html;
                var template = Handlebars.compile(source);
                //var html = template(context);
                var content = $('#content .main-menu').html(template(data));
                behaviorSetter(content);
            }               
        });
        //var source = $('#t-categorires').html();
        //var template = Handlebars.compile(source);
        //var html = template(context);
        //console.log(html);
    }
    var showCategories = function(data){
        categories = data;
    	$.ajax({
            url: 'templates/categories.html', //ex. js/templates/mytemplate.handlebars
            cache: true,
            success: function(html) {
                var source = html;


                var template = Handlebars.compile(source);
		    	//var html = template(context);
		    	var content = $('#content .main-menu').html(template(categories));
                setCategoriesBehavior(content);
            }               
        });
    	//var source = $('#t-categorires').html();
    	//var template = Handlebars.compile(source);
    	//var html = template(context);
    	//console.log(html);
    }
    
    var setCategoriesBehavior = function($template){
        $('li a',$template).each(function(){
            $(this).click(function(e){
                procesUrlRequest(e,$(this));
            });
        });
    }
    var procesUrlRequest = function(e,$element){
        if(window.history.pushState){
            //For browsers which support pushstate
            e.preventDefault();
            var url = $element.attr('href');
            window.history.pushState({},"New title", url);
            urlDataHandler();
        }
    }

}(window.globalApp = window.globalApp || {}));

document.addEventListener('DOMContentLoaded',globalApp.init,false); 